import os
import sys
import PySide2

from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from fbs_runtime.application_context.PySide2 import ApplicationContext
from PySide2.QtWebEngineWidgets import QWebEngineView as QWebView,QWebEnginePage as QWebPage
from PySide2.QtWebEngineWidgets import QWebEngineSettings as QWebSettings
class Player(QWebView):

	def __init__(self, parent=None):
		QWebView.__init__(self, parent)

	def contextMenuEvent(self, event):
		pass

# appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext		
app = QApplication(sys.argv)
view = QWebView()
view.load(QUrl.fromLocalFile(os.path.abspath(os.path.join(os.path.dirname(__file__), "index.html"))))
view.show()
exit_code = app.exec_()
# 2. Invoke appctxt.app.exec_()
sys.exit(exit_code)
