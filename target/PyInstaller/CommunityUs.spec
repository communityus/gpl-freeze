# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['/home/desktop/temp/src/main/python/main.py'],
             pathex=['/home/desktop/temp/target/PyInstaller'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=['/home/desktop/temp/venv/lib/python3.6/site-packages/fbs/freeze/hooks'],
             runtime_hooks=['/tmp/tmpv8pz3tga/fbs_pyinstaller_hook.py'],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='CommunityUs',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               upx_exclude=[],
               name='CommunityUs')
